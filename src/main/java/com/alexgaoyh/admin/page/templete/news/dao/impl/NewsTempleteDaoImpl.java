package com.alexgaoyh.admin.page.templete.news.dao.impl;

import org.springframework.stereotype.Repository;

import com.alexgaoyh.admin.page.templete.news.dao.NewsTempleteDao;
import com.alexgaoyh.admin.page.templete.news.entity.NewsTemplete;
import com.alexgaoyh.common.dao.impl.BaseDaoImpl;

/**
 * 
 * @desc 新闻页面-管理模板dao接口实现类
 *
 * @author alexgaoyh
 * @Thu Oct 16 18:16:40 CST 2014
 */
@Repository
public class NewsTempleteDaoImpl extends BaseDaoImpl<NewsTemplete> implements NewsTempleteDao {
	

}
